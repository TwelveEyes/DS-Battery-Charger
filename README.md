### Notes
---
- Requires an inserted voidstone to function.
- Recharges non-full batteries in inventory when in active mode.
- Recharges internal voidstone when in passive mode.
- Charging the internal voidstone accelerates rift activity.
- Charging is split between non-full batteries. The more non-full batteries the slower the charging.
- Flesh & Steel only.
